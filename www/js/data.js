var events = {};
var gaPlugin;
t = new Date(),
today = ((t.getMonth() + 1) < 10 ? '0' + (t.getMonth() + 1) : (t.getMonth() + 1)) + '-' + (t.getDate() < 10 ? '0' + t.getDate() : t.getDate()) + '-' +t.getFullYear();
events[today] = [{content: 'TODAY', allDay: true, recordatorio:0}];
document.addEventListener("deviceready", onDeviceReady, false);
window.appRootDirName = "PILSEN";
function onDeviceReady() {
    window.localStorage.setItem("edicion", 0);
    var db = window.openDatabase("test", "1.0", "Test DB", 1000000);
    db.transaction(populateDB, errorCB, successCB);
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
    function populateDB(tx) {
        //tx.executeSql('DROP TABLE IF EXISTS DEMO');
        //tx.executeSql('DROP TABLE IF EXISTS DEMO2');
        tx.executeSql('CREATE TABLE IF NOT EXISTS DEMO (ID INTEGER PRIMARY KEY, dia_inicio VARCHAR, mes_inicio VARCHAR, ano_inicio VARCHAR,hora_inicio VARCHAR,minutos_inicio VARCHAR,dia_fin VARCHAR, mes_fin VARCHAR, ano_fin VARCHAR,hora_fin VARCHAR,minutos_fin VARCHAR,titulo VARCHAR,descripcion VARCHAR,repeticion VARCHAR,recordatorio VARCHAR,chica VARCHAR)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS DEMO2 (ID INTEGER PRIMARY KEY, chica VARCHAR, tipoCalendario VARCHAR, imagen VARCHAR, fechaNacimiento VARCHAR)');
        //tx.executeSql('INSERT INTO DEMO2 (ID, chica, tipoCalendario, imagen, fechaNacimiento) VALUES (null, 2,1,4,"")');
        //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
    }

    function errorCB(err) {
        //alert("Error processing SQL: "+err.code);
    }

    function successCB() {
        //alert("success!");
        db.transaction(queryConfig, errorCB);
        db.transaction(queryDates, errorCB);
        
    }

    
    function queryDates(tx){
        tx.executeSql ('SELECT * FROM DEMO', [], successDate, errorCB);
    }
    function queryConfig(tx){
        tx.executeSql ('SELECT * FROM DEMO2 WHERE ID=1', [], successConfig, errorCB);
    }
    function successDate(tx, results) {
        for (var i = 0; i < results.rows.length; i++) {
            //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+results.rows.item(i).mes_inicio+"-"+results.rows.item(i).dia_inicio+"-"+results.rows.item(i).ano_inicio);
            var hora_inicio =results.rows.item(i).hora_inicio;
            var minutos_inicio = results.rows.item(i).minutos_inicio;
            var hora_fin = results.rows.item(i).hora_fin;
            var minutos_fin = results.rows.item(i).minutos_fin;
            var mes_inicio = results.rows.item(i).mes_inicio;
            var dia_inicio = results.rows.item(i).dia_inicio;
            var ano_inicio = results.rows.item(i).ano_inicio;
            var ano_fin = results.rows.item(i).ano_fin;
            var dia_fin = results.rows.item(i).dia_fin;
            var mes_fin = results.rows.item(i).mes_fin;
            var startDate = (mes_inicio < 10 ? '0' + (mes_inicio) : (mes_inicio)) + "-" + ((dia_inicio) < 10 ? '0' + (dia_inicio) : (dia_inicio)) + "-" + ano_inicio;
            var endDate = mes_fin+"-"+dia_fin+"-"+ano_fin;
            var dtstart_calendario = hora_inicio+":"+minutos_inicio;
            var dtend_calendario = hora_fin+":"+minutos_fin;
            var title = results.rows.item(i).titulo;
            var ID=results.rows.item(i).ID;
            var repeticion = results.rows.item(i).repeticion;
            //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+startDate);
            
            if((startDate in events)){
              events[startDate].push({content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion});
            }else{
              events[startDate]=[{content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion}];
            }
        };
    }
    function successConfig(tx, results) {
        if (results.rows.length > 0) {
            for (var i = 0; i < results.rows.length; i++) {
                var chica = results.rows.item(i).chica;
                var tipoCalendario = results.rows.item(i).tipoCalendario;
                var imagen = results.rows.item(i).imagen;
                var ID=results.rows.item(i).ID;
                window.localStorage.setItem("Calendario",tipoCalendario);
                window.localStorage.setItem("chicaSeleccionada",chica);
                window.localStorage.setItem("fotoSeleccion",imagen);
            };
            window.wakeuptimer.consulta( successConsulta, errorConsulta,{});
            //console.log("ID:"+ID+"tipoCalendario:"+tipoCalendario+"imagen:"+imagen+"chica:"+chica);
        }else{
            location.href = "#/main";
            /*jQuery( document ).ready(function( $ ) {
              $("body .loader_ini").hide();
            });*/
        }
    }

    function error (argument) {
        //alert("error:"+argument);
    }
//The directory to store data
function fail() {
    //alert("failed to get filesystem");
}

function gotFS(fileSystem) {
    //alert("filesystem got");
    window.fileSystem = fileSystem;
    fileSystem.root.getDirectory(window.appRootDirName, {
        create : true,
        exclusive : false
    }, dirReady, fail);
}

function dirReady(entry) {
    window.appRootDir = entry;

    //console.log("application dir is ready");
}

//gaPlugin = window.plugins.gaPlugin;
//gaPlugin.init(successHandler, errorHandler, "UA-57584978-12", 5);
//window.analytics.startTrackerWithId('UA-57584978-12');
var analytics = navigator.analytics;

//set the tracking id
analytics.setTrackingId('UA-57584978-12');

analytics.sendAppView('Home', successHandler, errorHandler);

//or the same could be done using the send function

var Fields    = analytics.Fields,
 HitTypes  = analytics.HitTypes,
 LogLevel  = analytics.LogLevel,
 params    = {};
params[Fields.HIT_TYPE]     = HitTypes.APP_VIEW;
params[Fields.SCREEN_NAME]  = 'Home';
analytics.setLogLevel(LogLevel.INFO);
analytics.send(params, successHandler, errorHandler);

window.plugins.NativeAudio.preloadSimple( 'Grabiela', 'alarmas/Grabiela.mp3', function(msg){
}, function(msg){
    //console.log( 'error: ' + msg );
});

window.plugins.NativeAudio.preloadSimple( 'Karen', 'alarmas/Karen.mp3', function(msg){
}, function(msg){
    //console.log( 'error: ' + msg );
});

}



function successConsulta(evento){
    if(evento.type == "wakeup"){
        //console.log("wilfrido1:"+evento.message);
        if(evento.chica == "Grabiela"){
            window.plugins.NativeAudio.loop( 'Grabiela' );
        }else if(evento.chica == "Karen"){
            window.plugins.NativeAudio.loop( 'Karen' );
        }
        window.localStorage.setItem("tituloAlarma", evento.titulo);
        window.localStorage.setItem("HoraAlarma", evento.hora);
        window.localStorage.setItem("MinutosAlarma", evento.minutos);
        window.localStorage.setItem("FormatoAlarma", evento.formato);
        location.href = "#/alarma";
    }else if(evento){
        var obj = JSON.parse(evento);
        //console.log("wilfrido2:"+obj.message);

        if(obj.chica == "Grabiela"){
            window.plugins.NativeAudio.loop( 'Grabiela' );
        }else if(obj.chica == "Karen"){
            window.plugins.NativeAudio.loop( 'Karen' );
        }
        window.localStorage.setItem("tituloAlarma", obj.titulo);
        window.localStorage.setItem("HoraAlarma", obj.hora);
        window.localStorage.setItem("MinutosAlarma", obj.minutos);
        window.localStorage.setItem("FormatoAlarma", obj.formato);
        location.href = "#/alarma";
    }else{
        //console.log("es nulo");
        location.href = "#/tab/home";
    }
}
function errorConsulta(evento){
    //console.log(evento);
}


function successHandler(){
	
}
function errorHandler(){
	
}
function successDespertador(evento){
  alert("Alarma Creada");
  //console.log("success");
}
function errorDespertador(evento){
  //console.log(evento);
}
function realarmaSuccess(evento){
    //alert("Alarma Creada");
    //console.log("success");
}
function realarmaError(evento){
    //console.log(evento);
}